@extends('layouts.app')
@section('kategori')
  @foreach ($kategori as $items)
    <a class="dropdown-item" href="{{ url('bKategori/'.$items->id) }}">{{$items->nama}}</a>      
  @endforeach
@endsection
@section('content')
  <div class="container">    
    <div class="card">
      @foreach ($produk as $item)      
      <div class="card-header">        
      <h3>{{ $item->nama }}</h3>
      </div>
      <div class="card-body">
      @foreach ($item->kategori as $items)
        {{ $items->nama }}
      @endforeach
      <br>
        <img class="card-img-top" src="{{ asset('fotoProduk/'.$item->foto)}}" alt="Card image cap">        
        {{ $item->deskripsi }}        
      </div>  
      @endforeach
    </div>
    
  </div>
  
@endsection