@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <form action="{{ route('kategori.store') }}" method="POST">
                        @csrf
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Nama</label>
                      <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukan nama Kategori" name="nama">
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection