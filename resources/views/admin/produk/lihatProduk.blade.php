@extends('layouts.app')
@section('content')
    <div class="container">
      <div class="card">
        <div class="card-header">
          <h3>{{$produk->nama}}</h3>
        </div>
        <img class="card-img-top" src="{{ asset('fotoProduk/'.$produk->foto)}}" alt="Card image cap">
        <div class="card-body">
          <p class="card-text">{{$produk->deskripsi}}</p>
          Kategori : 
          @foreach ($produk->kategori as $item)

              {{ $item->nama }}
              
          @endforeach
          <br>
          <a href="{{ url('produk') }}">Kembali</a>

        </div>
      </div>
    </div>
@endsection