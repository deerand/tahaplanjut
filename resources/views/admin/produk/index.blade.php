@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Daftar Produk
    </div>
    <div class="card-body">
      <a href="{{ url('produk/create') }}" class="btn btn-info" style="margin-bottom:20px;">Tambah Data</a>
      @foreach ($kategori as $item)
        <a href="{{ url('lihatBerdasarkanKategori/'.$item->id) }}" class="btn btn-secondary" style="margin-bottom:20px;">{{$item->nama}}</a>          
      @endforeach
      <div class="table-responsive">
        <table class="table" id="produk-table" width="100%">
          <thead>
            <tr>
              <td>ID</td>
              <td>Nama</td>
              <td>Actions</td>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <td>ID</td>
              <td>Nama</td>
              <td>Actions</td>
            </tr>
          </tfoot>          
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="deleteProdukModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id" id="id">
        <div class="infoText">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" data-idDel="" type="button" id="deleteProduk">Delete</button>
      </div>
    </div>
  </div>
</div>
@endsection