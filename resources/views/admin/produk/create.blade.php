@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <form action="{{ route('produk.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Nama</label>
                      <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukan Nama Produk" name="nama">
                    </div>
                    @foreach ($kategori as $item)
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" name="kategori[]" id="inlineCheckbox{{$item->id}}" value="{{ $item->id }}">
                      <label class="form-check-label" for="inlineCheckbox{{$item->id}}">{{ $item->nama }}</label>
                    </div>                        
                    @endforeach
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Foto</label>
                      <input type="file" class="form-control" name="foto">
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Deskripsi</label>
                      <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control"></textarea>
                    </div>                    
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection