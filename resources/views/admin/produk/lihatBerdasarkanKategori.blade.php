@extends('layouts.app')
@section('kategori')
  @foreach ($kategoris as $items)
    <a class="dropdown-item" href="{{ url('bKategori/'.$items->id) }}">{{$items->nama}}</a>      
  @endforeach
@endsection
@section('content')
  <div class="container">    
    <div class="card">
      @foreach($kategori as $item)
        <div class="card-header">        
            {{ $item->nama }}
        </div>
      <div class="card-body">
          <img class="card-img-top" src="{{ asset('fotoProduk/'.$item->foto)}}" alt="Card image cap">        
        {{ $item->deskripsi }}          
      </div>  
      @endforeach 
    </div>
    
  </div>
  
@endsection