<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProdukModel;
use App\KategoriModel;
use DataTables;
use Image;
use DB;
use Illuminate\Support\Str;
use File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = KategoriModel::select(['id','nama'])->get();
        return view('admin.produk.index',['kategori' => $kategori]);
    }
    public function lihatBerdasarkanKategori($id)
    {
    $kategoris = KategoriModel::select(['id','nama'])->get();
    $kategori = ProdukModel::whereHas('kategori', function($q) use ($id){

        $q->where('kategori.id', '=',$id);
    })->get();        
    return view('admin.produk.lihatBerdasarkanKategori',['kategori' => $kategori,
    'kategoris' => $kategoris]);
    }
    public function getProduk()
    {
        $getProduk = ProdukModel::select(['id', 'nama']);

        return Datatables::of($getProduk)
            ->addColumn('action', function ($produk) {
                return '<button type="button" id="deleteProduk" class="btn btn-xs btn-danger" data-id="'.$produk->id.'">Delete</button>
                <a href="produk/'.$produk->id.'/edit" class="btn btn-xs btn-primary">Edit</a>
                <a href="lihatProduk/'.$produk->id.'/" class="btn btn-xs btn-info">Lihat Produk</a>';
            })
            ->make(true);
    }
    public function lihatProduk($id)
    {
        $produk = ProdukModel::findOrFail($id);
        return view('admin.produk.lihatProduk',['produk' => $produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = KategoriModel::select(['id','nama'])->get();
        return view('admin.produk.create', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        if($request->hasFile('foto'))
        {
            $originalImage = $request->file('foto');
            //Get Filename with The Extension
            $filenameWithExt = $request->file('foto')->getClientOriginalName();

            //Get Just Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            
            //Get Just Ext
            $extension = $request->file('foto')->getClientOriginalExtension();

            $slugNamaProduk = Str::slug($request->input('nama'), '_');
            //Filename to Store
            $filenameToStore = $slugNamaProduk.'_'.str_random(8).'.'.$extension;
            
            $thumbnailImage = Image::make($originalImage);            
            $originalPath = public_path().'/fotoProduk/';
            $thumbnailImage->save($originalPath.$filenameToStore);

            $produk = new ProdukModel;
            $produk->nama = $request->input('nama');
            $produk->foto = $filenameToStore;
            $produk->deskripsi = $request->input('deskripsi');
            $produk->save();
            
            $produk->kategori()->sync($request->kategori, false);
            return redirect('produk');
        } else {
            $produk = new ProdukModel;
            $produk->nama = $request->input('nama');
            $produk->foto = 'default_image.jpg';
            $produk->deskripsi = $request->input('deskripsi');
            $produk->save();
            $produk->kategori()->sync($request->kategori, false);
            return redirect('produk');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = ProdukModel::findOrFail($id);
        return response()->json($produk,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = ProdukModel::findOrFail($id);
        $getKategoriId = DB::table('produk_kategori')->where('produk_id','=',$id)->pluck('kategori_id')->all();
        $kategori = KategoriModel::all();
        return view('admin.produk.edit',[
            'produk' => $produk,
            'kategori'=> $kategori,
            'getKategoriId' => $getKategoriId
        ]);
    }    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('foto'))
        {
            $originalImage = $request->file('foto');
            //Get Filename with The Extension
            $filenameWithExt = $request->file('foto')->getClientOriginalName();

            //Get Just Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            
            //Get Just Ext
            $extension = $request->file('foto')->getClientOriginalExtension();

            $slugNamaProduk = Str::slug($request->input('nama'), '_');
            //Filename to Store
            $filenameToStore = $slugNamaProduk.'_'.str_random(8).'.'.$extension;
            
            $thumbnailImage = Image::make($originalImage);            
            $originalPath = public_path().'/fotoProduk/';
            $thumbnailImage->save($originalPath.$filenameToStore);

            $produk = ProdukModel::findOrFail($id);
            $deleteFoto = public_path().'/fotoProduk/'.$produk->foto;
            File::delete($deleteFoto);
            $produk->nama = $request->input('nama');
            $produk->foto = $filenameToStore;
            $produk->deskripsi = $request->input('deskripsi');
            $produk->save();
            
            if (isset($request->kategori)) {
                $produk->kategori()->sync($request->kategori);
            } else {
                $produk->kategori()->sync(array());
            }
            return redirect('produk');
        } else {
            $produk = ProdukModel::findOrFail($id);
            $produk->nama = $request->input('nama');            
            $produk->deskripsi = $request->input('deskripsi');
            $produk->save();
            if (isset($request->kategori)) {
                $produk->kategori()->sync($request->kategori);
            } else {
                $produk->kategori()->sync(array());
            }
            return redirect('produk');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = ProdukModel::findOrFail($id);
        $deleteFoto = public_path().'/fotoProduk/'.$produk->foto;
        File::delete($deleteFoto);
        $produk->kategori()->wherePivot('produk_id','=',$id)->detach();
        $produk->delete();

    }
}
