<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriModel;
use App\ProdukModel;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['welcome','bKategori']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function welcome()
    {
        $kategori = KategoriModel::select(['id','nama'])->get();
        $produk = ProdukModel::all();
        return view('welcome', ['produk' => $produk,
        'kategori' => $kategori]);        
    }
    public function bKategori($id) {
        $kategoris = KategoriModel::select(['id','nama'])->get();
        $kategori = ProdukModel::whereHas('kategori', function($q) use ($id){

            $q->where('kategori.id', '=',$id);
        })->get();        
        return view('front.pengunjung.bKategori',['kategori' => $kategori,
        'kategoris' => $kategoris]);
    }
}
