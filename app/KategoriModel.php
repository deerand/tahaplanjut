<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    protected $table = 'kategori';
    protected $fillable = [
        'nama'
    ];

    public function produk()
    {
        return $this->belongsToMany('App\ProdukModel','produk_kategori','produk_id','kategori_id');
    }
}
