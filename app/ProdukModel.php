<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukModel extends Model
{
    protected $table = 'produk';
    protected $fillable = [
        'nama','foto','deskripsi'
    ];

    public function kategori()
    {
        return $this->belongsToMany('App\KategoriModel','produk_kategori','produk_id','kategori_id');        
    }
}
