$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });  
  $('#kategori-table').DataTable({
    processing: true,
    serverSide: true,  
    ajax: '/getKategori',
    columns: [
      { data: 'id', name: 'id' },
      { data: 'nama', name: 'nama' },
      { data: 'action', name: 'action' }
    ],
    responsive: true,
    initComplete: function () {
      this.api().columns([1]).every(function () {
        var column = this;
        var input = document.createElement("input");
        $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
      });
    }
  });
  $('#kategori-table').on('click', '#deleteKategori', function () {
    const dataId = $(this).data('id');
    $.ajax({
      url: 'kategori/' + dataId,
      type: 'GET',
      success: function (data)
      {
        $('#deleteKategoriModal').modal({
          show: true
        })
        $('.infoText').html('<p>Apakah ada ingin menghapus data ini? <strong>' + data.nama + '</strong></p>');
        $('#id').val(dataId)
      }
    })
    $('#deleteKategoriModal').modal({
      show:true
    })
  })
  $('#deleteKategori').on('click', function () {
    const getId = $('#id').val();
    $.ajax({
      url: 'kategori/' + getId,
      type: 'DELETE',
      success: function ()
      {
        $('#deleteKategoriModal').modal('hide');
        $('#kategori-table').DataTable().ajax.reload(null, false).draw();
      }
    })
  })
  $('#produk-table').DataTable({
    processing: true,
    serverSide: true,  
    ajax: '/getProduk',
    columns: [
      { data: 'id', name: 'id' },
      { data: 'nama', name: 'nama' },
      { data: 'action', name: 'action' }
    ],
    responsive: true,
    initComplete: function () {
      this.api().columns([1]).every(function () {
        var column = this;
        var input = document.createElement("input");
        $(input).appendTo($(column.footer()).empty())
          .on('change', function () {
            column.search($(this).val(), false, false, true).draw();
          });
      });
    }
  });
  $('#produk-table').on('click', '#deleteProduk', function () {
    const dataId = $(this).data('id');
    $.ajax({
      url: 'produk/' + dataId,
      type: 'GET',
      success: function (data)
      {
        console.log(data);
        $('#deleteProdukModal').modal({
          show: true
        })
        $('.infoText').html('<p>Apakah ada ingin menghapus data ini? <strong>' + data.nama + '</strong></p>');
        $('#id').val(dataId)
      }
    })
    $('#deleteProdukModal').modal({
      show:true
    })
  })
  $('#deleteProduk').on('click', function () {
    const getId = $('#id').val();
    $.ajax({
      url: 'produk/' + getId,
      type: 'DELETE',
      success: function ()
      {
        $('#deleteProdukModal').modal('hide');
        $('#produk-table').DataTable().ajax.reload(null, false).draw();
      }
    })
  })  
});