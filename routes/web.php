<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/bKategori/{id}', 'HomeController@bKategori')->name('bKategori');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('kategori', 'KategoriController');
    Route::get('getKategori','KategoriController@getKategori');
    Route::resource('produk', 'ProdukController');
    Route::get('getProduk','ProdukController@getProduk');
    Route::get('lihatProduk/{id}','ProdukController@lihatProduk');
    Route::get('lihatBerdasarkanKategori/{id}','ProdukController@lihatBerdasarkanKategori');
});